const messageTreatment = require('./messageTreatment');
const http = require('http');
const hostname = '127.0.0.1';
const port = 3003;
const user = 'Marcos';

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(messageTreatment.returnUserMessageNewFile(user));
});

server.listen(port, hostname, () => {
console.log('Server is running at://', hostname, ':', port);
});

function returnMessageToUser(user){
    if (user == "Vinicius"){
        return `Hi ${user}, you're a developer!`
    }
    return "User not allowed to see page's content."
}


//Docummentation: https://nodejs.org/en/docs/guides/getting-started-guide/